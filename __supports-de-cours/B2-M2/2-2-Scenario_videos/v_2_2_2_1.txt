Scénario v_2_2_2_1.txt
Titre : Etudes des langages de programmation (introduction)
==========================================================



Nous avons vu la partie du langage Python normalement suffisante pour réaliser les codes demandés dans le cours NSI.

Prenons maintenant un peu de recul dans l'étude des langages de programmation.

Comment sont-ils définis ?

Quels sont les élements communs à différents langages et propre à chacun ?

Quels sont les mécanismes utilisés en programmation impérative et orienté-objets ?

Quels sont les paradigmes sur lesquels reposent un langage de programmation ?

Comme une telle étude est titanesque, nous ne donnerons ici qu'un résumé des points qui nous semblent importants.

Commençons, dans la vidéo suivante,  par expliquer ce que l'on entend par paradigme de programmation. 

Notons que les vidéos que nous vous proposons sont des résumés et morceaux choisis du support écrit
qui lui même est, en grande partie, un résumé des cours de

"Langages de programmation" (volumes I et II)
du professeur Yves Roggeman,

donné en référence dans les notes. 
