Nous pouvons maintenant généraliser ce principe aux nombres réels[^1].
Commençons par observer qu’un nombre rationnel qui possède une
expression finie dans une base ne possède pas forcément une expression
finie dans toutes les bases. Prenons par exemple le nombre
$\frac{1}{3}$. Nous savons qu’il n’est possible de l’exprimer de manière
finie en base $10$, mais bien de manière infinie périodique:

$$\frac{1}{3}=0,3333_{10}\ldots$$

Nous notons cette représentation infinie périodique en soulignant la
partie qui se répète infiniment souvent:

$$\frac{1}{3}=0,\underline{3}.$$

Par contre, comme $\frac{1}{3}=3^{-1}$, on peut exprimer $\frac{1}{3}$
de manière finie en base $3$:

$$\frac{1}{3}=0,1_{3}.$$

Voyons maintenant comment représenter un nombre $N<1$ dans une base $b$
arbitraire. Notons que nous supposons que $N<1$ car on peut traiter
séparément la partie entière et la partie fractionnelle d’un nombre.
Pour exprimer $N$ en base $b$, nous allons procéder par *multiplication
successive* par $b$. Supposons que $N$ est de la forme:

$$\begin{aligned}
  N &= 0,\alpha_0\end{aligned}$$

où $\alpha_0$ représente la séquence de chiffres de la partie
fractionnaire du nombre. En multipliant $N$ par $b$ on obtient un
nouveau nombre de la forme:

$$\begin{aligned}
  N\times b &= \beta_1,\alpha_1\end{aligned}$$

où $\beta_1$ est la partie entière, et $\alpha_1$ la partie décimale.
Comme nous avons supposé que $N<1$, nous savons que
$0\leq \beta_1 \leq b-1$. Considérons le nouveau nombre $0,\alpha_1$, et
appelons-le $N_1$. Nous avons donc :

$$\begin{aligned}
  N_1 &= 0,\alpha_1\\
  N &= \frac{\beta_1}{b} + \frac{N_1}{b}.&              [4]\end{aligned}$$

Multiplions maintenant $N_1$ par $b$, nous obtenons un nombre de la
forme:

$$\begin{aligned}
  N_1\times b &= \beta_2,\alpha_2 \end{aligned}$$

avec $0\leq \beta_2 \leq b-1$. Nous pouvons à nouveau poser $N_2$ et
obtenir:

$$\begin{aligned}
  N_2 &= 0,\alpha_2\\
  N_1 &= \frac{\beta_2}{b} + \frac{N_2}{b}.&               [5]\end{aligned}$$

En combinant $[5]$ et $[4]$, nous obtenons:

$$\begin{aligned}
  N &= \frac{\beta_1}{b} + \frac{\frac{\beta_2}{b} + \frac{N_2}{b}}{b}\\
    &= \frac{\beta_1}{b} +\frac{\beta_2}{b^2} + \frac{N_2}{b^2}\\
    &= \beta_1\times b^{-1} + \beta_2\times b^{-2} + N_2\times b^{-2}.\end{aligned}$$

Nous pouvons continuer ce développement, en multipliant successivement
tous les $N_i=0,\alpha_i$ par $b$, pour obtenir un suite aussi longue
que souhaitée de $\beta_i$ tels que:

$$\begin{aligned}
  N &= \beta_1\times b^{-1} + \beta_2\times b^{-2} +\cdots
      \beta_k\times b^{-k} + N_k\times b^{-k}\\
  &=\sum_{i=1}^{k} \beta_i b^{-i}+ N_k\times b^{-k}&         [6].\end{aligned}$$

En comparant $[6]$ et $[1]$, on voit que la séquence des $\beta_i$ ainsi
calculée correspond à la séquence des $d_{-i}$ nécessaires pour exprimer
$N$ en base $b$. Ce développement sera arrêté soit lorsque $N_i=0$, soit
dès qu’on détecte deux valeurs $N_j$, $N_i$ ($i\neq j$) telles que
$N_j=N_i$, ce qui signifie que l’expression de $N$ en base $b$ est
infinie périodique.

**Exemple :**

Considérons $N=-24,42_{10}$ à exprimer en base $2$. Nous allons
commencer par exprimer $24$ en base $2$, en divisant successivement $24$
par $2$. On commence par $24/2=12$ avec un reste de $0$. Autrement dit
$q_0=12$ et $r_0=0$. On poursuit en divisant $q_0$ par $2$, etc:

| $i$ | $q_i$ | $r_i$ |
|:---:|:-----:|:-----:|
|  0  |  12   |   0   |
|  1  |   6   |   0   |
|  2  |   3   |   0   |
|  3  |   1   |   1   |
|  4  |   0   |   1   |

On lit maintenant la séquence des restes de haut en bas, en on obtient:
$24_{10} = 11000_2$.

Pour la partie fractionnaire, on procède par multiplication successive
par $2$. On commence par $0,42\times 2=0,84$. Autrement dit, $\beta_1=0$
et $N_1=0,84$. On multiplie à nouveau $N_1$ par $2$, soit
$2\times 0,84= 1,68$. On obtient donc:

<div class="center" markdown="1">

| $i$ | $\beta_i$ | $N_i$ |
|:---:|:---------:|:-----:|
|  1  |     0     | 0,84  |
|  2  |     1     | 0,68  |
|  3  |     1     | 0,36  |
|  4  |     0     | 0,72  |
|  5  |     1     | 0,44  |
|  6  |     0     | 0,88  |
|  7  |     1     | 0,76  |
|  8  |     1     | 0,52  |
|  9  |     1     | 0,04  |
| 10  |     0     | 0,08  |
| 11  |     0     | 0,16  |
| 12  |     0     | 0,32  |
| 13  |     0     | 0,64  |
| 14  |     1     | 0,28  |
| 15  |     0     | 0,56  |
| 16  |     1     | 0,12  |
| 17  |     0     | 0,24  |
| 18  |     0     | 0,48  |
| 19  |     0     | 0,96  |
| 20  |     1     | 0,92  |
| 21  |     1     | 0,84  |

</div>

On voit donc que $N_{21}=N_1$, la suite du développement se poursuivra
donc *ad infinitum* de manière cyclique. Donc:

$$0,42_{10}=0,0\underline{110\ 1011\ 1000\ 0101\ 0011}_2$$

et finalement:

$$-24,42_{10} = - 1\ 1000, 0\underline{110\ 1011\ 1000\ 0101\ 0011}_2.$$

$\blacksquare$

[^1]: En pratique, sur un ordinateur, nous ne pourrons représenter et
    manipuler de manière précise que les nombres qui possèdent une
    représentation finie en base $2$, ce qui exclut d’office tous les
    nombres irrationnels, comme $\pi$ ou $\sqrt{2}$, par exemple. Mais
    cela n’empêche que même les nombres irrationnels possèdent aussi une
    représentation (infinie non-périodique) dans d’autres bases que la
    base $10$.
