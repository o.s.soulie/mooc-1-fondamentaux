## Conception d'une base de données 2/5 : le modèle Entité-Association

> **Supports complémentaires**
>
> -   [Diapositives: le modèle entité / association](http://sql.bdpedia.fr/files/slea.pdf)  
> -   [Vidéo sur le modèle entité / association](https://mediaserver.cnam.fr/videos/le-modele-entiteassociation/)  

Cette session présente une méthode complète pour aboutir à un schéma
relationnel normalisé. Complète ne veut pas dire infaillible : *aucune*
méthode ne produit automatiquement un résultat correct, puisqu'elle
repose sur un processus d'analyse et d'identification des entités dont
rien ne peut garantir la validité par rapport à un besoin souvent
insuffisamment précis.

Le modèle Entité/Association (E/A) propose essentiellement une notation
pour soutenir la démarche de conception de schéma présentée
précédemment. La notation E/A a pour caractéristiques d'être simple et
suffisamment puissante pour modéliser des structures relationnelles. De
plus, elle repose sur une représentation graphique qui facilite sa
compréhension.

###  Le schéma de la base *Films*

La présentation qui suit est délibérement axée sur l'utilité du modèle
E/A dans le cadre de la conception d'une base de données. Ajoutons
qu'il ne s'agit pas directement de *concevoir* un schéma E/A (voir un
cours sur les systèmes d'information), mais d'être capable de le
comprendre et de l'interpréter. Nous reprenons l'exemple d'une base
de données décrivant des films, avec leur metteur en scène et leurs
acteurs, ainsi que les cinémas où passent ces films. Nous supposerons
également que cette base de données est accessible sur le Web et que des
internautes peuvent noter les films qu'ils ont vus.

La méthode permet de distinguer les *entités* qui constituent la base de
données, et les *associations* entre ces entités. Un schéma E/A décrit
l'application visée, c'est-à-dire une *abstraction* d'un domaine
d'étude, pertinente relativement aux objectifs visés. Rappelons qu'une
abstraction consiste à choisir certains aspects de la réalité perçue (et
donc à éliminer les autres). Cette sélection se fait en fonction de
certains *besoins*, qui doivent être précisément définis, et rélève
d'une démarche d'analyse qui n'est pas abordée ici.

<img src="films.png" alt="figure 21" width="500px">

>  *Fig. 21* : Le schéma E/A des films

Par exemple, pour notre base de données *Films*, on n'a pas besoin de
stocker dans la base de données l'intégralité des informations
relatives à un internaute, ou à un film. Seules comptent celles qui sont
importantes pour l'application. Voici le schéma décrivant cete base de
données *Films* (figure Fig.21).
Sans entrer dans les détails pour l'instant, on distingue

> -   des *entités*, représentées par des rectangles, ici *Film*,
>     *Artiste*, *Internaute* et *Pays* ;
> -   des *associations* entre entités représentées par des liens entre
>     ces rectangles. Ici on a représenté par exemple le fait qu'un
>     artiste *joue* dans des films, qu'un internaute *note* des films,
>     etc.

Chaque entité est caractérisée par un ensemble d'attributs, parmi
lesquels un ou plusieurs forment l'identifiant unique (en gras). Il est
essentiel de dire ce qui caractérise de manière unique une entité, de
manière à éviter la redondance d'information. Comme nous l'avons
préconisé précédemment, un attribut non-descriptif a été ajouté à chaque
entité, indépendamment des attributs "descriptifs". Nous l'avons
appelé **id** pour *Film* et *Artiste*, **code** pour le pays. Le nom de
l'attribut-identifiant est peu important, même si la convention **id**
est très répandue.

Seule exception : les internautes sont identifiés par un de leurs
attributs descriptifs, leur adresse de courrier électronique. Même s'il
s'agit en apparence d'un choix raisonnable (unicité de l'email pour
identifier une personne), ce cas nous permettra d'illustrer les
problèmes qui peuvent quand même se poser.

Les associations sont caractérisées par des *cardinalités*. La notation
0..\* sur le lien *Réalise*, du côté de l'entité *Film*, signifie
qu'un artiste peut réaliser plusieurs films, ou aucun. La notation 0..1
du côté *Artiste* signifie en revanche qu'un film ne peut être réalisé
que par au plus un artiste. En revanche dans l'association *Donne une
note*, un internaute peut noter plusieurs films, et un film peut être
noté par plusieurs internautes, ce qui justifie l'a présence de 0..\*
aux deux extrémités de l'association.

Le choix des cardinalités est *essentiel*. Ce choix est aussi parfois
discutable, et constitue donc l'un des aspects les plus délicats de la
modélisation. Reprenons l'exemple de l'association *Réalise*. En
indiquant qu'un film est réalisé par *un seul* metteur en scène, on
s'interdit les \-- pas si rares \-- situations où un film est réalisé
par plusieurs personnes. Il ne sera donc pas possible de représenter
dans la base de données une telle situation. Tout est ici question de
choix et de compromis : est-on prêt en l'occurrence à accepter une
structure plus complexe (avec 0..\* de chaque côté) pour l'association
*Réalise*, pour prendre en compte un nombre minime de cas ?

Les cardinalités sont notées par deux chiffres. Le chiffre de droite est
la *cardinalité maximale*, qui vaut en général 1 ou \*. Le chiffre de
gauche est la cardinalité minimale. Par exemple la notation 0..1 entre
*Artiste* et *Film* indique qu'on s'autorise à ne pas connaître le
metteur en scène d'un film. Attention : cela ne signifie pas que ce
metteur en scène n'existe pas. Une base de données, telle qu'elle est
décrite par un schéma E/A, ne prétend pas donner une vision exhaustive
de la réalité. On ne doit surtout pas chercher à *tout* représenter,
mais s'assurer de la prise en compte des besoins de l'application.

La notation 1..1 entre *Film* et *Pays* indique au contraire que l'on
doit toujours connaître le pays producteur d'un film. On devra donc
interdire le stockage dans la base d'un film sans son pays.

Les cardinalités minimales sont moins importantes que les cardinalités
maximales, car elles ont un impact limité sur la structure de la base de
données et peuvent plus facilement être remises en cause après coup. Il
faut bien être conscient de plus qu'elles ne représentent qu'un choix
de conception, souvent discutable. Dans la notation UML que nous
présentons ici, il existe des notations abrégées qui donnent des valeurs
implicites aux cardinalités minimales :

> -   La notation \* est équivalente à 0..\* ;
> -   la notation 1 est équivalente à 1..1 .

Outre les propriétés déjà évoquées (simplicité, clarté de lecture),
évidentes sur ce schéma, on peut noter aussi que la modélisation
conceptuelle est totalement indépendante de tout choix d'implantation.
Le schéma de la figure *Fig.21 Le schéma E/A des films* ne spécifie aucun système en particulier. Il n'est pas non plus question
de type ou de structure de données, d'algorithme, de langage, etc. En
principe, il s'agit donc de la partie la plus stable d'une
application. Le fait de se débarrasser à ce stade de la plupart des
considérations techniques permet de se concentrer sur l'essentiel : que veut-on stocker dans la base ?

Une des principales difficultés dans le maniement des schémas E/A est
que la qualité du résultat ne peut s'évaluer que par rapport à une
demande qui est difficilement formalisable. Il est donc souvent
difficile de mesurer (en fonction de quels critères et quelle métrique ?) l'adéquation du résultat au besoin. Peut-on affirmer par exemple que
:

> -   *toutes* les informations nécessaires sont représentées ?
> -   qu'un film ne sera *jamais* réalisé par plus d'un artiste ?

Il faut faire des choix, en connaissance de cause, en sachant toutefois qu'il est toujours possible de faire évoluer une base de données, quand cette évolution n'implique pas de restructuration trop importante. Pour reprendre les exemples ci-dessus, il est facile d'ajouter des informations pour décrire un film ou un internaute ; il serait beaucoup
plus difficile de modifier la base pour qu'un film passe de un, et un seul, réalisateur, à plusieurs. Quant à changer l'identifiant de la table *Internaute*, c'est une des évolutions les plus complexes à réaliser. Les cardinalités et le choix des clés font vraiment partie des aspects décisifs des choix de conception.

**Entités, attributs et identifiants**


Il est difficile de donner une définition très précise des entités. Les points essentiels sont résumés par la définition ci-dessous.


> **Définition : Entités**  
> On désigne par *entité* toute unité d'information *identifiable* et *pertinente* pour l'application.


La notion d'unité d'information correspond au fait qu'une entité ne peut pas se décomposer sans perte de sens. Comme nous l'avons vu précédemment, *l'identité* est primordiale. C'est elle qui permet de distinguer les entités les unes des autres, et donc de dire qu'une information est redondante ou qu'elle ne l'est pas. Il est indispensable de prévoir un moyen technique pour pouvoir effectuer cette
distinction entre entités au niveau de la base de données : on parle
*d'identifiant* ou (dans un contexte de base de données) de *clé*.
Reportez-vous au chapitre *Le modèle relationnel*
pour une définition précise de cette notion.

La pertinence est également importante : on ne doit prendre en compte
que les informations nécessaires pour satisfaire les besoins. Par exemple :

> -   le film *Impitoyable* ; 
> -   l'acteur *Clint Eastwood* ;

sont des entités pour la base *Films*.

La première étape d'une conception consiste à identifier les entités utiles. On peut souvent le faire en considérant quelques cas
particuliers. La deuxième est de regrouper les entités en ensembles : en général on ne s'intéresse pas à un individu particulier mais à des groupes. Par exemple il est clair que les films et les acteurs constituent des ensembles distincts d'entités. Qu'en est-il de l'ensemble des réalisateurs et de l'ensemble des acteurs ? Doit-on les distinguer ou les assembler ? Il est certainement préférable de les
assembler, puisque des acteurs peuvent aussi être réalisateurs.

**Attributs**

Les entités sont caractérisées par des *attributs* (ou *propriétés*) : le titre (du film), le nom (de l'acteur), sa date de naissance, l'adresse, etc. Le choix des attributs relève de la même démarche d'abstraction qui a dicté la sélection des entités : il n'est pas nécessaire de donner exhaustivement tous les attributs d'une entité. On
ne garde que ceux utiles pour l'application.

Un attribut est désigné par un *nom* et prend sa valeur dans un domaine comme les entiers, les chaînes de caractères, les dates, etc.

Un attribut peut prendre une valeur et une seule. On dit que les attributs sont *atomiques*. Il s'agit d'une restriction importante puisqu'on s'interdit, par exemple, de définir un attribut *téléphones* d'une entité *Personne*, prenant pour valeur *les* numéros de téléphone d'une personne. Cette restriction est l'une des limites du modèle relationnel, qui mène à la multiplication des tables par le mécanisme de
normalisation décrit en début de chapitre. Pour notre exemple, il faudrait par exemple définir une table dédiée aux numéros de téléphone et associée aux personnes.


> **Note**. 
> Certaines méthodes admettent l'introduction de constructions plus complexes :  
> -   les *attributs multivalués* sont constitués d'un *ensemble* de valeurs prises dans un même domaine ; une telle construction permet de résoudre le problème des numéros de téléphones multiples ;  
> -   les *attributs composés* sont constitués par agrégation d'autres attributs ; un attribut *adresse* peut par exemple être décrit comme l'agrégation d'un code postal, d'un numéro de rue, d'un nom de rue et d'un nom de ville.

> Cette modélisation dans le modèle conceptuel (E/A) doit pouvoir être transposée dans la base de données. Certains systèmes relationnels (PostgreSQL par exemple) autorisent des attributs complexes. Une autre solution est de recourir à d'autres modèles, semi-structurés ou objets.


Nous nous en tiendrons pour l'instant aux attributs atomiques qui, au moins dans le contexte d'une modélisation orientée vers un SGBD relationnel, sont suffisants.

### Types d'entités

Il est maintenant possible de décrire un peu plus précisément les entités par leur *type*.

> **Définition : Type d'entité**   
> Le type d'une entité est composé des éléments suivants :    
> -   son nom ;  
> -   la liste de ses attributs avec, \-- optionnellement \--le domaine
>     où l'attribut prend ses valeurs : les entiers, les chaînes de
>     caractères ;
> -   l'indication du (ou des) attribut(s) permettant d'identifier
>     l'entité : ils constituent la *clé*.


On dit qu'une entité *e* est une *instance* de son type *E*. Enfin, un ensemble d'entités $\{e_1, e_2, \ldots e_n\}$, instances d'un même type $E$ est une *extension* de $E$.

Rappelons maintenant la notion de clé, pratiquement identique à celle énoncée pour les schémas relationnels.

> **Définition : clé**   
> Soit $E$ un type d'entité et $A$ l'ensemble des attributs de $E$. Une *clé* de $E$ est un sous-ensemble *minimal* de $A$ permettant d'identifier de manière unique une entité parmi n'importe quelle extension de $E$.


Prenons quelques exemples pour illustrer cette définition. Un internaute est caractérisé par plusieurs attributs : son email, son nom, son prénom, la région où il habite. L'adresse mail constitue une clé naturelle puisqu'on ne trouve pas, en principe, deux internautes ayant la même adresse électronique. En revanche l'identification par le nom seul paraît impossible puisqu'on constituerait facilement un ensemble
contenant deux internautes avec le même nom. On pourrait penser à utiliser la paire `(nom,prénom)`, mais il faut utiliser avec modération l'utilisation d'identifiants composés de plusieurs attributs. Quoique possible, elle peut poser des problèmes de performance et complique les manipulations par SQL.

Il est possible d'avoir plusieurs clés candidates pour un même ensemble d'entités. Dans ce cas on en choisit une comme *clé principale* (ou primaire), et les autres comme clés secondaires. Le choix de la clé (primaire) est déterminant pour la qualité du schéma de la base de données. Les caractéristiques d'une bonne clé primaire sont les
suivantes :

> -   elle désigne sans ambiguité une et une seule entité dans toute
>     extension;
> -   sa valeur est connue pour toute entité ;
> -   on ne doit jamais avoir besoin de la modifier ;
> -   enfin, pour des raisons de performance, sa taille de stockage doit
>     être la plus petite possible.

Il est très difficile de trouver un ensemble d'attributs satisfaisant ces propriétés parmi les attributs descriptifs d'une entité.
Considérons l'exemple des films. Le choix du titre pour identifier un film serait incorrect puisqu'on aura affaire un jour ou l'autre à deux films ayant le même titre. Même en combinant le titre avec un autre attribut (par exemple l'année), il est difficile de garantir l'unicité.

Le choix de l'adresse électronique (email) pour un internaute semble respecter ces conditions, du moins la première (unicité). Mais peut-on vraiment garantir que l'email sera connu au moment de la création de l'entité? De plus, il semble clair que cette adresse peut changer, ce qui va poser de gros problèmes puisque la clé, comme nous le verrons,
sert à référencer une entité. Changer l'identifiant de l'entité
implique donc de changer *aussi* toutes les références. La conclusion s'impose: ce choix d'identifiant est un mauvais choix, il posera à terme des problèmes pratiques.

Insistons : la seule solution saine et générique consiste à créer un identifiant artificiel, indépendant de tout autre attribut. On peut ainsi ajouter dans le type d'entité *Film* un attribut `id`, correspondant à un numéro séquentiel qui sera incrémenté au fur et à mesure des insertions. Ce choix est de fait le meilleur, dès lors qu'un attribut ne respecte pas les conditions ci-dessus (autrement dit,
toujours). Il satisfait ces conditions : on peut toujours lui attribuer une valeur, il ne sera jamais nécessaire de la modifier, et elle a une représentation compacte.

On représente graphiquement un type d'entité comme sur la fig. 22 qui donne l'exemple des types *Internaute* et *Film*. L'attribut (ou les attributs s'il y en a plusieurs) formant la clé sont en gras.


<img src="entite.png" alt="figure 22" width="500px">

>  *Fig. 22* Représentation des types d'entité

Il est important de bien distinguer *types d'entités* et *entités*. La distinction est la même qu'entre entre *type* et *valeur* dans un langage de programmation, ou *schéma* et *base* dans un SGBD.

### Associations binaires

La représentation (et le stockage) d'entités indépendantes les unes des autres est de peu d'utilité. On va maintenant décrire les *relations* (ou *associations*) entre des ensembles d'entités.

> **Définition : association**   
> Une association binaire entre les ensembles d'entités $E_1$ et $E_2$ est un ensemble de couples $(e_1, e_2)$, avec $e_1 \in E_1$ et $e_2 \in E_2$.

C'est la notion classique, ensembliste, de relation. On emploie plutôt le terme d'association pour éviter toute confusion avec le modèle relationnel. Une bonne manière d'interpréter une association entre des ensembles d'entités est de faire un petit graphe où on prend quelques exemples, les plus généraux possibles.

<img src="graphe-ea.png" alt="figure 23" width="500px">

 > *Fig. 23*  Association entre deux ensembles

Prenons l'exemple de l'association représentant le fait qu'un
réalisateur met en scène des films. Sur le graphe de la fig.23  on remarque que :

> -   certains réalisateurs mettent en scène plusieurs films ;
> -   inversement, un film est mis en scène par au plus un réalisateur.

La recherche des situations les plus générales possibles vise à
s'assurer que les deux caractéristiques ci-dessus sont vraies dans tous les cas. Bien entendu on peut trouver *x* % des cas où un film a plusieurs réalisateurs, mais la question se pose alors : doit-on modifier la structure de notre base, pour *x* % des cas. Ici, on a décidé que non. Encore une fois on ne cherche pas à représenter la réalité dans toute sa complexité, mais seulement la partie de cette réalité que l'on veut stocker dans la base de données.

Ces caractéristiques sont essentielles dans la description d'une association entre des ensembles d'entités.


> **Définition : cardinalités**     
> Soit une association (E_1, E_2) entre deux types d’entités. La cardinalité de l’association pour  (E_i, i \in \{1, 2\}  , est une paire [min, max] telle que :

>  - Le symbole *max* (cardinalité maximale) désigne le nombre *maximal* de fois où une
une entité e_i peut intervenir dans l’association.
> - En général, ce nombre est 1 (au plus une fois) ou *n*  (plusieurs fois, nombre indeterminé), noté
par le symbole *.
> - Le symbole *min*  (cardinalité minimale) désigne  le nombre <em>minimal</em> de fois où une une entité e_i   peut intervenir dans l’association.

En général, ce nombre est 1 (au moins une fois) ou 0.


Les cardinalités maximales sont plus importantes que les cardinalités minimales ou, plus précisément, elles s'avèrent beaucoup plus difficiles à remettre en cause une fois que le schéma de la base est constitué. On décrit donc souvent une association de manière abrégée en omettant les cardinalités minimales. La notation *\** en UML, est l'abréviation de *0..\**, et *1* est l'abréviation de *1..1*. On caractérise également une association de manière concise en donnant les
cardinalités maximales aux deux extrémités, par exemple *1:\**
(association de un à plusieurs) ou *\*:\** (association de plusieurs à plusieurs).

Les cardinalités minimales sont parfois désignées par le terme
*contraintes de participation*. La valeur 0 indique qu'une entité peut ne pas participer à l'association, et la valeur 1 qu'elle doit y participer.

Insistons sur le point suivant : *les cardinalités n'expriment pas une vérité absolue, mais des choix de conception*. Elles ne peuvent être déclarés valides que relativement à un besoin. Plus ce besoin sera exprimé précisément, et plus il sera possible d'appécier la qualité du modèle.

Il existe plusieurs manières de noter une association entre types d'entités. Nous utilisons ici la notation de la méthode UML. En France, on utilise aussi couramment \-- de moins en moins \-- la notation de la méthode MERISE que nous ne présenterons pas ici.

<img src="assoc.png" alt="figure 24" width="500px">

> *Fig 24* .Représentation de l'association

Dans la notation UML, on indique les cardinalités aux deux extrémités d'un lien d'association entre deux types d'entités $T_A$ et $T_B$.
Les cardinalités pour $T_A$ sont placées à l'extrémité du lien allant de $T_A$ à $T_B$ et les cardinalités pour $T_B$ sont l'extrémité du lien allant de $T_B$ à $T_A$.

Pour l'association entre *Réalisateur* et *Film*, cela donne
l'association de la fig.24. Cette association se lit *Un réalisateur réalise zéro, un ou plusieurs films*,
mais on pourrait tout aussi bien utiliser la forme passive avec comme intitulé de l'association *Est réalisé par* et une lecture *Un film est réalisé par au plus un réalisateur*. Le seul critère à privilégier dans ce choix des termes est la clarté de la représentation.

Prenons maintenant l'exemple de l'association *(Acteur, Film)*
représentant le fait qu'un acteur joue dans un film. Un graphe basé sur quelques exemples est donné dans la fig. 25. On constate tout d'abord qu'un acteur peut jouer dans
plusieurs films, et que dans un film on trouve plusieurs acteurs. Mieux : Clint Eastwood, qui apparaissait déjà en tant que metteur en scène, est maintenant également acteur, et dans le même film.

<img src="graphe-ea2.png" alt="figure 25" width="500px">

> * Fig. 25* Association *(Acteur,Film)*

Cette dernière constatation mène à la conclusion qu'il vaut mieux regrouper les acteurs et les réalisateurs dans un même ensemble, désigné par le terme plus général "Artiste". On obtient le schéma de la Fig.26, avec les deux associations
représentant les deux types de lien possible entre un artiste et un film : il peut jouer dans le film, ou le réaliser. Ce "ou" n'est pas exclusif : Eastwood joue dans *Impitoyable*, qu'il a aussi réalisé.

<img src="assoc2.png" alt="figure 26" width="500px">

>  *Fig. 26* Association entre *Artiste* et *Film*

Dans le cas d'associations avec des cardinalités multiples de chaque côté, on peut avoir des attributs qui ne peuvent être affectés qu'à l'association elle-même. Par exemple l'association *Joue* a pour attribut le rôle tenu par l'acteur dans le film
(Fig. 26).

Rappelons qu'un attribut ne peut prendre qu'une et une seule valeur.
Clairement, on ne peut associer `rôle` ni à *Acteur* puisqu'il a autant de valeurs possibles qu'il y a de films dans lesquels cet acteur a joué, ni à *Film*, la réciproque étant vraie également. Seules les associations ayant des cardinalités multiples de chaque côté peuvent porter des attributs.

Quelle est la clé d'une association ? Si l'on s'en tient à la
définition, une association est un *ensemble* de couples, et il ne peut donc y avoir deux fois le même couple (parce qu'on ne trouve pas deux fois le même élément dans un ensemble). On a donc :


> **Définition : Clé d'une association**        
> La clé d'une association (binaire) entre un type d'entité $E_1$ et un type d'entité $E_2$ est la paire constituée de la clé $c_1$ de $E_1$ et de la clé $c_2$ de $E_2$.  


Cette contrainte est parfois trop contraignante car on souhaite
autoriser deux entités à être liées plus d'une fois dans une
association.

<img src="assoc-internaute.png" alt="figure 27" width="500px">

>  Fig.  27 Association entre *Internaute* et *Film*

Prenons le cas de l'association entre *Internaute* et *Film*
(Fig. 27). Elle est identifiée par la paire `(idFilm, email)`. Imaginons par exemple qu'un internaute soit amené à noter à plusieurs reprises un film, et que l'on souhaite conserver l'historique de ces notations successives. Avec une
association binaire entre *Internaute* et *Film*, c'est impossible : on ne peut définir qu'un seul lien entre un film donné et un internaute donné.

Le problème est qu'il n'existe pas de moyen pour distinguer des liens multiples entre deux mêmes entités. Dans ce type de situation, on peut considérer que l'association est porteuse de trop sens et qu'il vaut mieux la *réifier* sous la forme d'une entité.

> ** Note**. 
> La réification consiste à transformer en un objet réel et autonome une idée ou un concept. Ici, le concept d'association entre deux entités est réifié en lui donnant le statut d'entité, avec pour principale conséquence l'attribution d'un identifiant autonome. Cela signifie qu'une telle entité pourrait, en principe, exister indépendamment des entités de l'association initiale. Quelques précautions s'imposent, mais en pratique, c'est une option tout à fait valable.

<img src="assoc-reifiee.png" alt="figure 28" width="500px">

*Fig 28*  réification de l'association entre *Internaute* et *Film*


La Fig.28  montre le modèle obtenu en transformant l'association *Note* en entité *Notation*. Notez soigneusement les caractéristiques de cette transformation :

> -   chaque note a maintenant un identifiant propre, `id`
> -   une entité *Note* est liée par une association "plusieurs à un"
>     respectivement à un film et à un internaute ; cela reflète
>     l'origine de cette entité, représentant un lien entre une paire
>     d'entités (Film, Internaute).
> -   Nous avons mis une contrainte de participation forte (1..1) pour
>     ces associations, afin de conserver l'idée qu'une note ne
>     saurait exister sans que l'on connaisse le film d'une part,
>     l'internaute d'autre part.

Le choix de réifier ou non une association plusieurs-plusieurs relève du jugement. Dès qu'une association porte beaucoup d'information et des contraintes un peu complexes, il est sans doute préférable de la transformer en entité.


> **Note**  
> Certains outils de conception ne permettent que les associations un-à-plusieurs, ce qui impose de fait de réifier les associations plusieurs-plusieurs.

