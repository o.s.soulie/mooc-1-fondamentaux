## Présentation du module : Architecture matérielle

[![Vidéo 1 B4-M1-S0 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-objectifs.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-objectifs.mp4)

## Présentation
[Support de la présentation](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/blob/master/bloc_4_architecture_robotique_reseaux_ihm/4-1_architecture_materielle/4-1-0_Pr%C3%A9sentation_du_module/4-1-0-2_Texte_Pr%C3%A9sentation.md) 

## Objectifs du module

L'objectif principal de ce module est de comprendre le fonctionnement d’un système informatique (microcontrôleur, ordinateur) pour être capable de sélectionner les caractéristiques d’une machine correspondant à une application donnée, de les exploiter et d'en appréhender les limites.
Le second objectif est d'acquérir les bases en architecture pour suivre les nombreuses évolutions technologiques de l'informatique, afin de pouvoir continuer à en exploiter les possibilités.


## Sommaire



    1. Introduction à l'architecture des systèmes
    2. Architecture minimale d'un système informatique
    3. Architecture des systèmes embarqués
    4. Architecture des ordinateurs
    5. Conférences



## Prérequis
Électronique numérique (numération, logique booléenne) 

## Temps d'apprentissage

* 12 à 15 heures (Ce temps est donné à titre indicatif. Il peut varier en fonction des participants.)

## Présentation enseignant

**Anthony Juton**

Professeur agrégé en Physique appliquée, Anthony Juton enseigne l'informatique industrielle au département Nikola Tesla de l'ENS Paris Saclay.

